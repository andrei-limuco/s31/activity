//Setup the dependancies
const express = require("express");
const mongoose = require("mongoose");
//This will allow us to use all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

//Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.iha4o.mongodb.net/B157_to-do?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
//Allow all the task routes created in the "taskRoute.js" file to use the "/taks" route
app.use("/tasks", taskRoute);
//http:localhost:4000/tasks/

app.listen(port, () => console.log(`Now listening to port ${port}`));