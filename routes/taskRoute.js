//Contains all of the endpoints of our application

//We need to use express' Router() function to achieve this
const express = require("express");
//Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//The "taskController" allows us to use the functions defined in the taskController.js file
const taskController = require("../controllers/taskController")

//Route to get all the tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => {
		res.send(resultFromController);
	}) 
});

//Route to create a task
//This route expects to receive POST request at the URL "/tasks/"
router.post("/", (req, res) => {
	console.log(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for deleting a Task
router.delete("/:id", (req, res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//Activity
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id, req.body).then(resultFromController => {
		res.send(resultFromController);
	}) 
});


router.put("/:id/complete", (req, res) => {
	taskController.changeTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

})




//Use "module.exports" to export the router object to use in the index.js
module.exports = router;
