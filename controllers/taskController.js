//Controllers coontain the functions and business logic of our Express JS app

const Task = require("../models/task");

//Controller function for getting all the tasks
//Defines the functions to be used in the "taskRoute.js" file and exports and these functions

module.exports.getAllTasks = () => {
	//model.method
	return Task.find({}).then(result => {
		return result;
	}) 
}

//Controller function for creating task
//The request body coming from the client was passed from the "taskRoute.js" file via "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

//Saves the newly created "newTask" object in our MongoDB Database
//The "then" method waits until the task is stored in the database or ab error is encountered before returning a "true" or "false" value back to the client
//The "then" method will accept 2 arguments
	//First parameter will store the result returned by the Mongoose "save" method
	//Second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task
		}
	})
}

//Controller for deleting a task
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}

	})

}

//Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}

//Activity
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	}) 
}


module.exports.changeTaskStatus = (taskId) => {

	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}

		result.status = "complete"

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}